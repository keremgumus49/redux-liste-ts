import { ListState, ListAction } from "../types/text";

const defaultState = {
  list: [],
};

const textReducer = (
  state: ListState = defaultState,
  action: ListAction
): ListState => {
  switch (action.type) {
    case "ADD":
      console.log("ahada ranza");
      return {
        list: [...state.list, action.payload],
      };
    case "RMV":
      return { list: [] };

    default:
      return state;
  }
};

export default textReducer;
