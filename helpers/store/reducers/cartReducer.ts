import { CartActions, CartState } from "../types/cart";

const defaultState: CartState = {
  item: [{ product: { id: "1", name: "a", price: "1" }, quantity: 1 }],
};

const cartReducer = (
  state: CartState = defaultState,
  action: CartActions
): CartState => {
  switch (action.type) {
    case "CART_ADD": {
      const itemIndex = state.item.findIndex(
        (item) => action.payload.product.id == item.product.id
      );

      if (itemIndex !== -1) {
        const qty = state.item[itemIndex].quantity;
        state.item[itemIndex].quantity = action.payload.quantity + qty;
        return state;
      } else {
        state.item.push(action.payload);
        return state;
      }
    }
    case "CART_DEC": {
    }
    default:
      return state;
  }
};
export default cartReducer;
