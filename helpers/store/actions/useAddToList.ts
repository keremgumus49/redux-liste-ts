import { useDispatch } from "react-redux";
type Props = {
  list: string;
};

export const useAddToList = (props: Props) => {
  const dispatch = useDispatch();

  return (payload: string) => dispatch({ type: "ADD", payload: props.list });
};

export const useRemoveList = () => {
  const dispatchh = useDispatch();
  return () => dispatchh({ type: "RMV" });
};
