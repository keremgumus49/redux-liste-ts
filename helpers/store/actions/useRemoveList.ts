import { useDispatch } from "react-redux";

export const useRemoveList = () => {
  const dispatchh = useDispatch();
  return () => dispatchh({ type: "RMV" });
};
