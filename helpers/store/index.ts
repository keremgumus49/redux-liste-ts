import { ListState } from "./types/text";
import { combineReducers } from "redux";
import textReducer from "./reducers/textReducer";
import { CartState } from "./types/cart";
import cartReducer from "./reducers/cartReducer";
import todoReducer from "./reducers/toDoReducer";
import { ToDoState } from "./types/toDo";

export interface AppState {
  list: ListState;
  cart: CartState;
  todo: ToDoState;
}

const rootReducer = combineReducers<AppState>({
  list: textReducer,
  cart: cartReducer,
  todo: todoReducer,
});

export default rootReducer;
