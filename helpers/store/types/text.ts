import { ThunkDispatch } from "redux-thunk";

export interface ListState {
  list: string[];
}

export interface TextEkle {
  type: "ADD" | "RMV";
  payload: string;
}

export type ListAction = TextEkle;
export type ListDispatch = ThunkDispatch<ListState, void, ListAction>;
