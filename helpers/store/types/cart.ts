import { ThunkDispatch } from "redux-thunk";

export type Cart = {
  product: { name: string; id: string; price: string };
  quantity: number;
};

export interface CartState {
  item: Cart[];
}

export interface CartActions {
  type: "CART_INC" | "CART_ADD" | "CART_DEC" | "CART_RMV";
  payload: Cart;
}

export type ListAction = CartActions;
export type ListDispatch = ThunkDispatch<CartState, void, ListAction>;
