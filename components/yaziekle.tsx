import { useState } from "react";
import { useAddToList } from "../helpers/store/actions/useAddToList";
import { Cart } from "../helpers/store/types/cart";

const Yazi = () => {
  const [formData, setFormData] = useState<string>("");
  const AddToList = useAddToList({ list: formData });

  const AddToListToggle = (metin: string) => {
    AddToList(metin);
  };
  return (
    <div>
      <label htmlFor="text">{"Enter text"} </label>
      <input
        type="text"
        onChange={(event) => setFormData(event.target.value)}
      />
      <button
        onClick={() => {
          AddToListToggle(formData);
        }}
      >
        {" Add Text "}
      </button>
    </div>
  );
};

export default Yazi;
